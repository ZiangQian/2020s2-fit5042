package fit5042.tutex.repository.entities;

/**
 * 
 * @author qianziang
 *
 */
public class Loan {
	private double principle;
	private double interestRate;
	private int numberOfYears;
	private double monthlyPayment;
	
	public Loan(double principle, double interestRate, int numberOfYears) {
		this.principle = principle;
		this.interestRate = interestRate;
		this.numberOfYears = numberOfYears;
	}

	public Loan() {
		this.principle = 0.0;
		this.interestRate = 0.0;
		this.numberOfYears = 0;
		this.monthlyPayment = 0.0;
	}

	public double getPrinciple() {
		return principle;
	}

	public void setPrinciple(double principle) {
		this.principle = principle;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public int getNumberOfYears() {
		return numberOfYears;
	}

	public void setNumberOfYears(int numberOfYears) {
		this.numberOfYears = numberOfYears;
	}

	public double getMonthlyPayment() {
		return monthlyPayment;
	}

	public void setMonthlyPayment(double monthlyPayment) {
		this.monthlyPayment = monthlyPayment;
	}

	@Override
	public String toString() {
		return "Loan [principle=" + principle + ", interestRate=" + interestRate+ ", numberOfYears=" + numberOfYears
				+ ", monthlyPayment=" + monthlyPayment + "]";
	}
	
	

}
