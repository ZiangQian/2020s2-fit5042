package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    
    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    public void createProperty() throws Exception {
    
    	// Hardcode 5 properties to repo
    	Property newProperty1 = new Property(1,"24 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 420000.00);
    	Property newProperty2 = new Property(2,"11 Bettina St, Clayton VIC 3168, Australia", 3, 352, 360000.00);
    	Property newProperty3 = new Property(3,"3 Wattle Ave, Glen Huntly VIC 3204, Australia", 5, 800, 650000.00);
    	Property newProperty4 = new Property(4,"3 Hamilton St, Bentleigh VIC 3204, Australia", 2, 170, 4435000.00);
    	Property newProperty5 = new Property(5,"82 Spring Rd, Hampton East VIC 3188, Australia", 1, 60, 820000.00);

    	propertyRepository.addProperty(newProperty1);
    	propertyRepository.addProperty(newProperty2);
    	propertyRepository.addProperty(newProperty3);
    	propertyRepository.addProperty(newProperty4);
    	propertyRepository.addProperty(newProperty5);
    	
    	System.out.println("5 properties  added successfully");
        
    }
    
    // this method is for displaying all the properties
    public void displayProperties() throws Exception {
    	
    	List<Property> list = propertyRepository.getAllProperties();
    	for (int i = 0; i < list.size(); i++) {
    		System.out.println(list.get(i).toString());
    	}
    	
    }
    
    // this method is for searching the property by ID
    public void searchPropertyById() throws Exception {
    	int id = AcceptInt("Enter the ID of the property you want to search: ");
    	List<Property> list = propertyRepository.getAllProperties();
    	boolean bl = false;
    	// This loop is to protect null point exception for property id
    	for (int i = 0; i < list.size(); i++) {
    		if (id == list.get(i).getId()) 
    		{
    			System.out.println(propertyRepository.searchPropertyById(id).toString());
    			bl = true;
    		}
    	}
    	if (bl == false) 
    	{
    		System.out.println("There is no property with the id");
    	}
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
    
    // This is a validations for making sure that the value is integer
    public static Integer AcceptInt(String words) throws Exception{
        Scanner input = new Scanner(System.in);
        System.out.println(words);
        int range;
        try
        {
        	while(true){
        		if(input.hasNextInt()){
        			range = input.nextInt();
        			if(0<=range && range <= 1000000000)
        				break;
        			else
        				continue;
        		}
        		input.nextLine();
        		System.out.println("Enter an integer");
        	}
        }
        finally 
        {
        	input.close();
        }
        return range;
    }
}
