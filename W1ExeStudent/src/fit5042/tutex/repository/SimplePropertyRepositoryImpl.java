/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	private ArrayList<Property> properties;
	
	public SimplePropertyRepositoryImpl() 
	{
		this.properties = new ArrayList<Property>();
	}

	public void addProperty(Property property) throws Exception {
		properties.add(property);
	}
	
	public Property searchPropertyById(int id) throws Exception {
		Property searchProperty = properties.get(id);
		return searchProperty;
	}
	
    public List<Property> getAllProperties() throws Exception {
    	List<Property> list = new ArrayList<>();
    	for (int i = 0; i < properties.size(); i++) {
    	list.add(properties.get(i));
    	}
		return list;
	}
    
}
